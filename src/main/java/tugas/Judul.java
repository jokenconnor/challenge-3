package tugas;

public abstract class Judul {
    private String judul;

    abstract void tampilan(String judul);

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }
}
