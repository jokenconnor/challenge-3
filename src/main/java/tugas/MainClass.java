package tugas;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

public class MainClass {

    public static void main(String[] args) throws Exception {
        menuUtama();

    }
    public static void menuUtama(){
        try {


        Scanner scan = new Scanner(System.in);
        int pilih;
        int pilih1;
        MainClass mainclass = new MainClass();

        System.out.println("---------------------------------------------");
        AnakJudul anakJudul = new AnakJudul();
        anakJudul.tampilan("Aplikasi Pengolah Nilai");
        System.out.println("---------------------------------------------");

        System.out.println("Pilih 1 untuk menghitung jumlah elemen");
        System.out.println("Pilih 2 untuk hitung mean, median dan modus");
        System.out.println("Pilih 3 untuk generate kedua file");

        System.out.print("Pilih menu: ");
        pilih = scan.nextInt();



        switch (pilih){
            case 1:
                mainclass.menuSatu();
                System.out.println("---------------------------------------------");
                anakJudul.tampilan("Aplikasi Pengolah Nilai");
                System.out.println("---------------------------------------------");
                System.out.println("Data telah di generate pada");
                System.out.println("D:\\Student Independent\\Challenge 2\\data_sekolah_modus.txt");
                System.out.println("0. exit");
                System.out.println("1. menu utama");
                pilih1 = scan.nextInt();

                if(pilih1 == 1){
                    menuUtama();
                }else if(pilih1 == 0){
                    System.exit(0);
                }


            case 2:
                mainclass.menuDua();
                System.out.println("---------------------------------------------");
                anakJudul.tampilan("Aplikasi Pengolah Nilai");
                System.out.println("---------------------------------------------");
                System.out.println("Data telah di generate pada");
                System.out.println("D:\\Student Independent\\Challenge 2\\data_sekolah_mean-median.txt");
                System.out.println("0. exit");
                System.out.println("1. menu utama");
                pilih1 = scan.nextInt();

                if(pilih1 == 1){
                    menuUtama();
                }else if(pilih1 == 0){
                    System.exit(0);
                }



            case 3:
                menuTiga();
                System.out.println("---------------------------------------------");
                anakJudul.tampilan("Aplikasi Pengolah Nilai");
                System.out.println("---------------------------------------------");
                System.out.println("Data telah digenerate dalam bentuk txt");
                System.out.println("0. exit");
                System.out.println("1. menu utama");
                pilih1 = scan.nextInt();

                if(pilih1 == 1){
                    menuUtama();
                }else if(pilih1 == 0){
                    System.exit(0);
                }



        }
        }
        catch (InputMismatchException e){
            System.err.println("Masukan Angka");
            menuUtama();
        }
        catch (FileNotFoundException e){
            System.err.println("File tidak ditemukan");
            menuUtama();
        }
        catch (Exception e) {
            System.out.println(e);
        }

        System.out.println(" ");



    }
    public float menuSatu()throws Exception{
        String path = "D:\\Student Independent\\Challenge 3\\src\\main\\java\\tugas\\data_sekolah.csv";

        File file = new File(path);
        FileReader fr = new FileReader(file);
        Scanner input = new Scanner(fr);

        String line = "";
        List<Integer> ls = new ArrayList<>();
        String[] tempArr;

        while (input.hasNextLine()) {
            line = input.nextLine();
            tempArr = line.split(";");
            for (String str : tempArr) {
                if (str.equals(tempArr[0])) {
                } else {
                    ls.add(Integer.valueOf(str));
                }

            }
        }

        Collections.sort(ls);


        int size = ls.size(); //ls itu data yg ditampung
        float modus = 0;
        int counter = 1;
        for(int i=0; i<size; i++){
            int frekuensi = Collections.frequency(ls,ls.get(i));
            if (frekuensi > counter) {
                modus = ls.get(i);
                counter = frekuensi;
            }
        }
        FileWriter writer = new FileWriter("data_sekolah_modus.txt");
        writer.write("Modus: "+modus);
        writer.write(System.getProperty("line.separator"));
        writer.close();
        return modus;

    }
    public double[] menuDua()throws Exception{
        String path = "D:\\Student Independent\\Challenge 3\\src\\main\\java\\tugas\\data_sekolah.csv";
        File file = new File(path);
        FileReader fr = new FileReader(file);
        Scanner input = new Scanner(fr);

        String line = "";
        List<Integer> ls = new ArrayList<>();
        String[] tempArr;

        while (input.hasNextLine()) {
            line = input.nextLine();
            tempArr = line.split(";");
            for (String str : tempArr) {
                if (str.equals(tempArr[0])) {
                } else {
                    ls.add(Integer.valueOf(str));
                }

            }
        }

        Collections.sort(ls);


        int size = ls.size();

        float mean = 0;
        for(float j : ls){
            mean = mean + j;

        }
        float hasil = Rumus.bagi(size, mean);

        float median;
        if(size%2==0){
            float x1 = ls.get(ls.size()/2);
            float x2 = ls.get(ls.size()/2-1);
            median = (x1 + x2)/2;
        }else{
            median = ls.get(ls.size()/2);
        }

        FileWriter writer = new FileWriter("data_sekolah_mean-median.txt");
        writer.write("Mean: "+hasil);
        writer.write(System.getProperty("line.separator"));
        writer.write("Median: "+median);
        writer.write(System.getProperty("line.separator"));

        writer.close();
        double[] mm = new double[2];
        mm[0] = hasil;
        mm[1] = median;

        return mm;
    }
    public static void menuTiga()throws Exception{
        MainClass mainclass = new MainClass();
        mainclass.menuSatu();
        mainclass.menuDua();

    }

}
