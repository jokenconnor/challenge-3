package tugas;

public class AnakJudul extends Judul{
    private String judul;

    @Override
    public void tampilan(String judul){
        this.judul = judul;

        System.out.println(judul);
    }

    @Override
    public String getJudul() {
        return judul;
    }

    @Override
    public void setJudul(String judul) {
        this.judul = judul;
    }
}
