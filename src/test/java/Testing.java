import org.junit.Assert;
import org.junit.Test;
import tugas.MainClass;

import java.io.IOException;
import java.util.Arrays;
import java.util.*;

public class Testing {
    MainClass mainclass = new MainClass();

    @Test
    public void testmenuSatu(){
        try {
            MainClass mainclass = new MainClass();
            Assert.assertEquals(7, mainclass.menuSatu(), 0);
            System.out.println("Program tidak terdapat error");
            System.out.println("Modus = " + mainclass.menuSatu());
        }
        catch(Exception e){
            System.out.println("Terdapat error pada program");
        }
    }

    @Test
    public void testmenuDua(){
        try {
            MainClass mainclass = new MainClass();
            Assert.assertEquals(8.3, mainclass.menuDua()[0], 0.1);
            Assert.assertEquals(8, mainclass.menuDua()[1], 0);
            System.out.println("Program tidak terdapat error ");
            System.out.println("Mean = " + mainclass.menuDua()[0]);
            System.out.println("Median = " + mainclass.menuDua()[1]);
        }
        catch(Exception e){
            System.err.println("Terdapat error pada program");
        }
    }

}
